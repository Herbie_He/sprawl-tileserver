![tileserver-gl](https://cloud.githubusercontent.com/assets/59284/18173467/fa3aa2ca-7069-11e6-86b1-0f1266befeb6.jpeg)

Instructions for serving tiles with Docker using the lastest Dockerfile from https://github.com/maptiler/tileserver-gl

# TileServer GL
[![GitHub Workflow Status](https://img.shields.io/github/actions/workflow/status/maptiler/tileserver-gl/pipeline.yml)](https://github.com/maptiler/tileserver-gl/actions/workflows/pipeline.yml)
[![Docker Hub](https://img.shields.io/badge/docker-hub-blue.svg)](https://hub.docker.com/r/maptiler/tileserver-gl/)

Vector and raster maps with GL styles. Server-side rendering by MapLibre GL Native. Map tile server for MapLibre GL JS, Android, iOS, Leaflet, OpenLayers, GIS via WMTS, etc.


## Getting Started with Docker
Make sure you have Node and Docker installed

clone this repository
```bash
git clone https://gitlab.com/Herbie_He/sprawl-tileserver.git
```
### Working with Local Machine
If you are wokring on a local machine, make sure you have installed and opened Docker.

### Working with Remote Machine
Add your current user to docker group (or ask your admin to do this)
```bash
sudo usermod -aG docker $USER
```
Switch session to docker group
```bash
newgrp - docker
```

### Build Docker Image
Run the following to build a Docker Image from Dockerfile and name the Docker Image as $DOCKER_IMAGE_NAME.
```bash
docker build -f Dockerfile . -t $DOCKER_IMAGE_NAME
```

Run the following to ensure the Docker Image is built sucessfully. You should see $DOCKER_IMAGE_NAME showing.
```bash
docker image ls
```
Before running this docker image, ensure you have these two files
- final.mbtiles
- config.json

Where final.mbtiles is the vector tiles that you are serving. You can download mbtiles for any country/region you like on [OpenStreetMap](https://data.maptiler.com/downloads/planet/) or generate SNDI mbtiles following [generate-tile](https://gitlab.com/cpbl/sprawl-website/-/tree/master/generate-tiles?ref_type=heads). Note that you should rename your mbtiles files to final.mbtiles so the Docker Image can locate it. Alternatively, you can also modify the "data">"openmaptiles">"mbtiles" attribute in the config.json to point to your mbtiles file.

Where config.json is a configuration file used by Docker Image to locate the mbtiles data and the map styling information. 
By default, the config.json is defined to display tiles for Cuba. If you want the see the effect of map styling on other regions, ensure you modify the attribute "styles">"$style_identifier">"tilejson">"bounds" in config.json to align with the bounds of your region. You can find the bounds of your region on [OpenStreetMap](https://data.maptiler.com/downloads/planet/). For example, if the final.mbtiles is the tiles for Tokyo, Japan, then you should set "styles">"$style_identifier">"tilejson">"bounds" to [138.779, 34.867, 141.152, 36.558] which is the bounds for Tokyo. 

You can also define the landing location of a certain map styling by defining the attribute "styles">"$style_identifier">"tilejson">"center". By default, all map styling lands in Cuba. Ensure your landing location aligns with the regions you're serving. For example, if you are serving mbtiles on Tokyo, Japan, ensure your "styles">"$style_identifier">"tilejson">"center" is set to somewhere in Tokyo, otherwise you will land in some bizzare location!

 You can explore more functionalities about the config.json file [here](https://tileserver.readthedocs.io/en/latest/config.html).


Once you have the mbtiles and config.json file, run the following command to run docker image. Where $DOCKER_IMAGE_NAME is the name of the Docker Image created earlier (use "docker image ls" to find the name of all Docker Images). 

```bash
docker run --rm -it -v $(pwd):/data -p 8080:8080 $DOCKER_IMAGE_NAME
```
or use the following to get back your command line
```bash
docker run -d --rm -it -v $(pwd):/data -p 8080:8080 $DOCKER_IMAGE_NAME
```

Check the status of the Container (a running instance of Docker Image) by running the following

```bash
docker ps
```
### Check result
 If the Container is running sucessfully, visit http://[server ip]:8080 to view the tileserver-gl interface. It should look something like this 
 ![tileserver-gl-interface](readme_images/tileserver-gl-interface.png)
 
 Click "Inspect" under the Data section to view the tiles. The following is what it looks like for the SNDI tiles for Cuba. See [generate-tile](https://gitlab.com/cpbl/sprawl-website/-/tree/master/generate-tiles?ref_type=heads) on how to generate SNDI tiles. 
![sndi-cuba-tiles](readme_images/sndi-cuba-tiles.png)



## Documentation

You can read the full documentation of this project at https://maptiler-tileserver.readthedocs.io/.


